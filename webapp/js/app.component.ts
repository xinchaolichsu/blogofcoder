import { Component } from '@angular/core';


@Component({
  selector: 'my-app',
  templateUrl: `<h3>Hello, {{ name }}</h3>`
})

export class AppComponent {
	name = 'Angular'; 
	username = ['Garung', 'Son Goken', 'Nguoi Du Bi'];

}
